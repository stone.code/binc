#include <cstdlib>

#include <llvm/ADT/ArrayRef.h>
#include <llvm/ADT/StringRef.h>
#include <llvm/IR/Constants.h>
#include <llvm/IR/Module.h>
#include <llvm/Support/MemoryBuffer.h>

#include "binc.h"

static llvm::Constant* getConstantDataArray( llvm::LLVMContext& context, llvm::ArrayRef<char> data ) {
#if LLVM_VERSION >= 700
	return llvm::ConstantDataArray::get( context, data );
#else
	return llvm::ConstantDataArray::get(
		context, llvm::ArrayRef<uint8_t>( reinterpret_cast<uint8_t const*>( data.data() ), data.size() ) );
#endif
}

static llvm::ErrorOr<llvm::Constant*> getInitializer( llvm::LLVMContext& context, llvm::StringRef filename,
													  bool nullTerminate, size_t* bufferSize ) {
	// Load the input data into a memory buffer.
	auto buffer = llvm::MemoryBuffer::getFileOrSTDIN( filename, -1, nullTerminate );
	if ( !buffer ) {
		return buffer.getError();
	}
	assert( bufferSize );
	*bufferSize = buffer->get()->getBufferSize();

	if ( nullTerminate ) {
		assert( *buffer->get()->getBufferEnd() == '\0' );
		auto data = llvm::ArrayRef<char>( buffer->get()->getBufferStart(), buffer->get()->getBufferSize() + 1 );
		return getConstantDataArray( context, data );
	}

	auto data = llvm::ArrayRef<char>( buffer->get()->getBufferStart(), buffer->get()->getBufferSize() );
	return getConstantDataArray( context, data );
}

llvm::ErrorOr<size_t> buildModule( llvm::Module& module, llvm::StringRef variableName, bool nullTerminate ) {
	size_t bufferSize = 0;
	auto& context = module.getContext();
	auto initializer = getInitializer( context, module.getSourceFileName(), nullTerminate, &bufferSize );
	if ( !initializer ) {
		return initializer.getError();
	}
	auto gv =
		new llvm::GlobalVariable( module, initializer.get()->getType(), true,
								  llvm::GlobalValue::LinkageTypes::ExternalLinkage, initializer.get(), variableName );
	std::array<llvm::Constant*, 2> idx = {llvm::ConstantInt::get( llvm::Type::getInt64Ty( context ), 0 ),
										  llvm::ConstantInt::get( llvm::Type::getInt64Ty( context ), bufferSize )};
	auto gv2 = llvm::ConstantExpr::getInBoundsGetElementPtr( gv->getType()->getPointerElementType(), gv, idx );
	llvm::GlobalAlias::create( llvm::Type::getInt8Ty( context ), 0, llvm::GlobalValue::LinkageTypes::ExternalLinkage,
							   variableName + "_end", gv2, &module );

	return bufferSize;
}
