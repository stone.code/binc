//------------------------------------------------------------------------------
//
// This file implements a binary asset compiler that is specific to x86_64 and
// linux.  This allows a much simpler implementation, and eliminates the in-
// memory copy of the asset required by the version built using LLVM.
//
// Benchmarking indicates that output file generation is limited by IO speed, so
// this tool is not any faster.  However, the use of a fixed size buffer during
// copying means that this approach can copy larger files without hitting memory
// exhaustion.
//
//------------------------------------------------------------------------------

#include <cassert>
#include <cstdint>
#include <cstring>
#include <llvm/BinaryFormat/ELF.h>
#include <llvm/Support/CommandLine.h>
#include <llvm/Support/FileSystem.h>
#include <llvm/Support/Path.h>
#include <llvm/Support/raw_os_ostream.h>

namespace cl = llvm::cl;

using std::string;

cl::OptionCategory category( "binc" );
cl::opt<string> OutputFilename( "o", cl::desc( "Specify the output filename.  If -, use stdout." ),
								cl::value_desc( "filename" ), cl::cat( category ) );
cl::opt<string> InputFilename( "c", cl::desc( "Specify the input filename.  If -, use stdin." ),
							   cl::value_desc( "filename" ), cl::Required, cl::init( "-" ), cl::cat( category ) );
cl::opt<string> VariableName( "n", cl::desc( "Specify linkage name for binary asset." ), cl::value_desc( "identifier" ),
							  cl::cat( category ) );
cl::opt<bool> NullTerminate( "z", cl::desc( "Add a null terminator to the binary asset." ), cl::init( true ),
							 cl::cat( category ) );

struct Layout {
	Layout( size_t data, size_t filename, size_t varname ) noexcept;
	Layout( Layout const& ) noexcept = default;
	~Layout() = default;

	size_t getDataSize() const {
		return gnuStack - rodata;
	};
	size_t getSymbolTableSize() const {
		return strtab - symtab;
	};

	// Don't need the header, it will always be at zero.
	size_t const rodata;
	size_t gnuStack;
	size_t symtab;
	size_t strtab;
	size_t sections;

	size_t nameFile;
	size_t nameBegin;
	size_t nameEnd;
	size_t strtabSize;
};

static size_t paddedSize( size_t data ) {
	if ( data % 0x10 == 0 ) {
		return data;
	}

	return data + ( 0x10 - ( data % 0x10 ) );
}

static char strtab_prefix[] = "\0.text\0.rodata\0.note.GNU-stack\0.symtab\0.strtab";

Layout::Layout( size_t data, size_t filename, size_t varname ) noexcept : rodata( 0x40 ) {
	// Layout the string table.
	nameFile = sizeof( strtab_prefix );
	nameBegin = nameFile + filename + 1;
	nameEnd = nameBegin + varname + 1;
	strtabSize = nameEnd + varname + 4 + 1;

	// Will add a null to ensure termination of the data.
	if ( NullTerminate ) {
		data += 1;
	}

	// Layout the file sections
	gnuStack = rodata + data;
	symtab = rodata + paddedSize( data );
	strtab = symtab + paddedSize( 4 * 0x18 /* size of each symbol entry */ );
	sections = strtab + paddedSize( strtabSize );
}

static uint8_t* writeWord( uint8_t* buffer, uint32_t value ) {
	*buffer++ = value & 0xff;
	*buffer++ = ( value >> 8 ) & 0xff;
	*buffer++ = ( value >> 16 ) & 0xff;
	*buffer++ = ( value >> 24 ) & 0xff;
	return buffer;
}

static uint8_t* writeHalf( uint8_t* buffer, uint16_t value ) {
	*buffer++ = value & 0xff;
	*buffer++ = ( value >> 8 ) & 0xff;
	return buffer;
}

static uint8_t* writeXword( uint8_t* buffer, uint64_t value ) {
	*buffer++ = value & 0xff;
	*buffer++ = ( value >> 8 ) & 0xff;
	*buffer++ = ( value >> 16 ) & 0xff;
	*buffer++ = ( value >> 24 ) & 0xff;
	*buffer++ = ( value >> 32 ) & 0xff;
	*buffer++ = ( value >> 40 ) & 0xff;
	*buffer++ = ( value >> 48 ) & 0xff;
	*buffer++ = ( value >> 56 ) & 0xff;
	return buffer;
}

static uint8_t* writeAddr( uint8_t* buffer, uint64_t value ) {
	return writeXword( buffer, value );
}

static uint8_t* writeOff( uint8_t* buffer, uint64_t value ) {
	return writeXword( buffer, value );
}

static uint8_t* writePadding( uint8_t* buffer, int padding ) {
	while ( padding > 0 ) {
		*buffer++ = 0;
		padding--;
	}
	return buffer;
}

static llvm::StringRef getOutputFilename() {
	if ( !OutputFilename.empty() ) {
		return OutputFilename;
	}

	if ( InputFilename == "-" ) {
		OutputFilename = "-";
		return OutputFilename;
	}

	OutputFilename = ( llvm::StringRef( InputFilename ) + ".o" ).str();
	return OutputFilename;
}

static llvm::StringRef getVariableName() {
	if ( !VariableName.empty() ) {
		return VariableName;
	}

	if ( InputFilename == "-" ) {
		fprintf( stderr, "warn: variable name not specified, and could not be guessed\n" );
		return "stdin";
	}

	VariableName = llvm::sys::path::filename( InputFilename ).str();
	std::replace( VariableName.begin(), VariableName.end(), '.', '_' );
	return VariableName;
}

static uint8_t* buildHeader( uint8_t* buffer, Layout const& layout ) {
	*buffer++ = 0x7f;
	*buffer++ = 'E';
	*buffer++ = 'L';
	*buffer++ = 'F';
	*buffer++ = llvm::ELF::ELFCLASS64;
	*buffer++ = llvm::ELF::ELFDATA2LSB;
	*buffer++ = 1 /* version */;
	*buffer++ = 0 /* target operating system */;
	*buffer++ = 0 /* abi version */;
	buffer = writePadding( buffer, 7 );
	buffer = writeHalf( buffer, llvm::ELF::ET_REL );
	buffer = writeHalf( buffer, llvm::ELF::EM_X86_64 /* can this be zero? */ );
	buffer = writeWord( buffer, llvm::ELF::EV_CURRENT );
	buffer = writeAddr( buffer, 0 /* e_entry */ );
	buffer = writeOff( buffer, 0 /* no program header table */ );
	buffer = writeOff( buffer, layout.sections );
	buffer = writeWord( buffer, 0 /* flags */ );
	buffer = writeHalf( buffer, 0x40 /* size of header */ );
	buffer = writeHalf( buffer, 0 /* size of program header table entry */ );
	buffer = writeHalf( buffer, 0 /* number of entries in the program header table */ );
	buffer = writeHalf( buffer, 0x40 /* size of a section header table entry */ );
	buffer = writeHalf( buffer, 6 /* number of section header entries */ );
	buffer = writeHalf( buffer, 1 /* string table sectino entry */ );
	return buffer;
}

static size_t writeData( llvm::raw_fd_ostream& out, char const* in ) {
	auto file = fopen( in, "r" );
	if ( !file ) {
		return 0;
	}

	size_t totalBytes = 0;

	size_t const bufferSize = 3 * 1024;
	char buffer[bufferSize];

	do {
		auto bytesRead = fread( buffer, 1, bufferSize, file );
		if ( bytesRead <= 0 ) {
			break;
		}

		out.write( buffer, bytesRead );
		totalBytes += bytesRead;
	} while ( true );

	fclose( file );

	if ( NullTerminate ) {
		out.write( "", 1 );
		totalBytes += 1;
	}

	return totalBytes;
}

static uint8_t* buildSymbolTable( uint8_t* buffer, Layout const& layout ) {
	// Null entry
	buffer = writePadding( buffer, llvm::ELF::SYMENTRY_SIZE64 );

	// symbol for thefile
	buffer = writeWord( buffer, layout.nameFile );
	*buffer++ = llvm::ELF::STB_LOCAL << 4 | llvm::ELF::STT_FILE;
	*buffer++ = 0;
	buffer = writeHalf( buffer, llvm::ELF::SHN_ABS );
	buffer = writeAddr( buffer, 0 );
	buffer = writeXword( buffer, 0 );

	// symbol for data
	buffer = writeWord( buffer, layout.nameBegin );
	*buffer++ = llvm::ELF::STB_GLOBAL << 4 | llvm::ELF::STT_OBJECT;
	*buffer++ = 0;
	buffer = writeHalf( buffer, 3 /* rodata section */ );
	buffer = writeAddr( buffer, 0 );
	buffer = writeXword( buffer, layout.getDataSize() );

	// symbol for end of data
	buffer = writeWord( buffer, layout.nameEnd );
	*buffer++ = llvm::ELF::STB_GLOBAL << 4 | llvm::ELF::STT_OBJECT;
	*buffer++ = 0;
	buffer = writeHalf( buffer, 3 /* rodata section */ );
	buffer = writeAddr( buffer, NullTerminate ? ( layout.getDataSize() - 1 ) : layout.getDataSize() );
	buffer = writeXword( buffer, NullTerminate ? 1 : 0 );

	return buffer;
}

static void writeStringTable( llvm::raw_fd_ostream& out, Layout const& layout ) {
	out.write( strtab_prefix, sizeof( strtab_prefix ) );

	llvm::StringRef fn = InputFilename;
	out.write( fn.data(), fn.size() );
	out.write( '\0' );
	auto vn = getVariableName();
	out.write( vn.data(), vn.size() );
	out.write( '\0' );
	auto vne = ( vn + "_end" ).str();
	out.write( vne.data(), vne.size() );
	out.write( '\0' );

	auto const padding = paddedSize( layout.strtabSize ) - layout.strtabSize;
	if ( padding ) {
		uint8_t buffer[10];
		writePadding( buffer, padding );
		out.write( reinterpret_cast<char*>( buffer ), padding );
	}
}

static uint8_t* buildSections( uint8_t* buffer, Layout const& layout ) {
	memset( buffer, 0, 0x40 /* size of section entry */ );
	buffer += 0x40;

	/* Write string table */
	buffer = writeWord( buffer, 39 );
	buffer = writeWord( buffer, llvm::ELF::SHT_STRTAB );
	buffer = writeXword( buffer, 0 );
	buffer = writeAddr( buffer, 0 );
	buffer = writeOff( buffer, layout.strtab );
	buffer = writeXword( buffer, layout.strtabSize );
	buffer = writeWord( buffer, 0 );
	buffer = writeWord( buffer, 0 );
	buffer = writeXword( buffer, 1 /* align */ );
	buffer = writeXword( buffer, 0 );

	/* Write text */
	buffer = writeWord( buffer, 1 );
	buffer = writeWord( buffer, llvm::ELF::SHT_PROGBITS );
	buffer = writeXword( buffer, llvm::ELF::SHF_ALLOC | llvm::ELF::SHF_EXECINSTR );
	buffer = writeAddr( buffer, 0 );
	buffer = writeOff( buffer, 0 );
	buffer = writeXword( buffer, 0 );
	buffer = writeWord( buffer, 0 );
	buffer = writeWord( buffer, 0 );
	buffer = writeXword( buffer, 4 /* align */ );
	buffer = writeXword( buffer, 0 );

	/* Write rodata */
	buffer = writeWord( buffer, 7 );
	buffer = writeWord( buffer, llvm::ELF::SHT_PROGBITS );
	buffer = writeXword( buffer, llvm::ELF::SHF_ALLOC );
	buffer = writeAddr( buffer, 0 );
	buffer = writeOff( buffer, layout.rodata );
	buffer = writeXword( buffer, layout.getDataSize() );
	buffer = writeWord( buffer, 0 );
	buffer = writeWord( buffer, 0 );
	buffer = writeXword( buffer, 1 /* align */ );
	buffer = writeXword( buffer, 0 );

	/* Write note GNU-stack */
	buffer = writeWord( buffer, 15 );
	buffer = writeWord( buffer, llvm::ELF::SHT_PROGBITS );
	buffer = writeXword( buffer, 0 );
	buffer = writeAddr( buffer, 0 );
	buffer = writeOff( buffer, layout.gnuStack );
	buffer = writeXword( buffer, 0 );
	buffer = writeWord( buffer, 0 );
	buffer = writeWord( buffer, 0 );
	buffer = writeXword( buffer, 1 /* align */ );
	buffer = writeXword( buffer, 0 );

	/* Symbol table */
	buffer = writeWord( buffer, 31 );
	buffer = writeWord( buffer, llvm::ELF::SHT_SYMTAB );
	buffer = writeXword( buffer, 0 );
	buffer = writeAddr( buffer, 0 );
	buffer = writeOff( buffer, layout.symtab );
	buffer = writeXword( buffer, layout.getSymbolTableSize() );
	buffer = writeWord( buffer, 1 /* link */ );
	buffer = writeWord( buffer, 2 /* two local symbols */ );
	buffer = writeXword( buffer, 1 /* align */ );
	buffer = writeXword( buffer, llvm::ELF::SYMENTRY_SIZE64 );

	return buffer;
}

int main( int argc, char const* argv[] ) {
	// Process the command line.
	cl::HideUnrelatedOptions( category );
	if ( !cl::ParseCommandLineOptions( argc, argv ) ) {
		return EXIT_FAILURE;
	}

	uint64_t dataSize;
	auto ec = llvm::sys::fs::file_size( InputFilename, dataSize );
	if ( ec ) {
		fprintf( stderr, "error: could not determine input file size: %s", ec.message().c_str() );
		return EXIT_FAILURE;
	}

	auto filename = getOutputFilename();
	llvm::raw_fd_ostream out( filename, ec, llvm::sys::fs::OpenFlags::F_None );
	if ( ec ) {
		fprintf( stderr, "error: could not open output: %s\n", ec.message().c_str() );
		return EXIT_FAILURE;
	}

	// Write out the header
	Layout layout( dataSize, InputFilename.size(), getVariableName().size() );
	uint8_t buffer[0x180];
	auto pos = buildHeader( buffer, layout );
	assert( pos - buffer == 0x40 );
	out.write( reinterpret_cast<char*>( buffer ), pos - buffer );

	// Write the rodata section
	auto writtenSize = writeData( out, InputFilename.c_str() );
	if ( writtenSize != dataSize + NullTerminate ? 1 : 0 ) {
		fprintf( stderr, "error: mismatch is rodata size and the binary asset\n" );
		return EXIT_FAILURE;
	}
	assert( writtenSize == layout.getDataSize() );
	pos = writePadding( buffer, paddedSize( writtenSize ) - writtenSize );
	out.write( reinterpret_cast<char*>( buffer ), pos - buffer );

	// Write the symbol table
	pos = buildSymbolTable( buffer, layout );
	assert( pos - buffer == 0x60 );
	out.write( reinterpret_cast<char*>( buffer ), pos - buffer );

	// Write the string table
	writeStringTable( out, layout );

	// Write the sections.
	pos = buildSections( buffer, layout );
	assert( pos - buffer == 0x180 );
	out.write( reinterpret_cast<char*>( buffer ), pos - buffer );

	return EXIT_SUCCESS;
}