# Configure the build
LLVM_CONFIG ?= llvm-config
LLVM_VERSION_STR := $(shell $(LLVM_CONFIG) --version)
LLVM_VERSION := $(subst .,,$(LLVM_VERSION_STR))
CLANG_TIDY ?= clang-tidy
BINC_VERSION_STR ?= development
CXXFLAGS = -g `$(LLVM_CONFIG) --cxxflags`
CXXFLAGS += -DLLVM_VERSION_STR="${LLVM_VERSION_STR}"
CXXFLAGS += -DLLVM_VERSION=${LLVM_VERSION}
CXXFLAGS += -DBINC_VERSION_STR="${BINC_VERSION_STR}"
CXXFLAGS += -DCXX=$(CXX)
LDFLAGS = -g `$(LLVM_CONFIG) --ldflags`
ifeq ($(STATIC),ON)
LIBS := `$(LLVM_CONFIG) --link-static --libs --system-libs`
else
LIBS := `$(LLVM_CONFIG) --libs support`
endif
ifeq ($(COVERAGE),ON)
CXXFLAGS += --coverage -fsanitize=address
LDFLAGS += --coverage -fsanitize=address
endif
ifeq ($(STRICT),ON)
CXXFLAGS += -Wall -Wextra
endif
ifeq ($(STATIC),ON)
CXXFLAGS += -static
LDFLAGS += -static
endif
SCOV ?= scov

# Configure paths
SRCDIR ?= .
VPATH = $(SRCDIR)
PREFIX ?= /usr/local
BINDIR ?= $(PREFIX)/bin

all: binc binc-x86_64-linux

binc: binc.o config.o debug.o module.o object.o
	$(CXX) -o $@ $(LDFLAGS) $^ $(LIBS)

binc-x86_64-linux: binc-x86_64-linux.o
	$(CXX) -o $@ $(LDFLAGS) $^ $(LIBS)

binc.check: binc_test.o catch.o config.o debug.o module.o
	$(CXX) -o $@ $(LDFLAGS) $^ $(LIBS)

%.o: %.cpp binc.h
	$(CXX) -o $@ $(CXXFLAGS) -c $<

catch.o: vendor/catch2/catch.cpp
	$(CXX) -o $@ $(CXXFLAGS) -c $<

static-analysis: binc.cpp config.cpp debug.cpp module.cpp object.cpp
	$(CLANG_TIDY) $^ -- $(CXXFLAGS)

check: binc.check binc
	./$<
ifeq ($(COVERAGE),ON)
	gcov -i -b *.cpp
	$(SCOV) -title="Binary Asset Compiler (binc)" -htmldir=./coverage -srcid=`git describe` -exclude=^vendor/ .
endif 

clean:
	-$(RM) *.o binc binc.check
	-$(RM) -rf coverage *.gcov *.gcda *.gcno

install: binc binc-x86_64-linux
	@mkdir -p $(BINDIR)/
	@cp -p $^ $(BINDIR)/
