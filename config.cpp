#include <cstdio>
#include <llvm/ADT/Triple.h>
#include <llvm/Support/Path.h>

#include "binc.h"

llvm::StringRef getOutputFilename( std::string& outputFilename, llvm::StringRef inputFilename, CodeGenFileType fileType,
								   char const* targetName, llvm::Triple const& triple ) {
	if ( !outputFilename.empty() ) {
		return outputFilename;
	}

	if ( inputFilename == "-" ) {
		outputFilename = "-";
		return outputFilename;
	}

	outputFilename = inputFilename.str();
	switch ( fileType ) {
	default:
	case CGFT_AssemblyFile:
		assert( targetName );
		if ( targetName[0] == 'c' ) {
			if ( targetName[1] == 0 ) {
				outputFilename += ".cbe.c";
			} else if ( targetName[1] == 'p' && targetName[2] == 'p' ) {
				outputFilename += ".cpp";
			} else {
				outputFilename += ".s";
			}
		} else {
			outputFilename += ".s";
		}
		break;

	case CGFT_ObjectFile:
		if ( triple.getOS() == llvm::Triple::Win32 ) {
			outputFilename += ".obj";
		} else {
			outputFilename += ".o";
		}
		break;

	case CGFT_Null:
		outputFilename += ".null";
		break;

	case CGFT_IR:
		outputFilename += ".ll";
		break;
	}
	return outputFilename;
}

llvm::StringRef getVariableName( std::string& variableName, llvm::StringRef inputFilename ) {
	if ( !variableName.empty() ) {
		return variableName;
	}

	if ( inputFilename.empty() || inputFilename == "-" ) {
		fprintf( stderr, "warn: variable name not specified, and could not be guessed\n" );
		return "stdin";
	}

	variableName = llvm::sys::path::filename( inputFilename ).str();
	std::replace( variableName.begin(), variableName.end(), '.', '_' );
	return variableName;
}
