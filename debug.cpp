#include <cassert>
#include <llvm/IR/DIBuilder.h>
#include <llvm/IR/Module.h>
#include <llvm/Support/Path.h>

#include "binc.h"

void buildDebugInfo( llvm::Module& module, size_t bufferSize ) {
	assert( !module.global_empty() );

	llvm::DIBuilder builder( module );

	// Compile unit
	auto const filename = module.getSourceFileName();
	auto file = builder.createFile( filename, llvm::sys::path::root_path( filename ) );
	builder.createCompileUnit( llvm::dwarf::DW_LANG_C, file, "binc", false, "", 0 );

	// Debug information for our one and only object.
	auto gv = module.global_begin();
	auto type1 = builder.createBasicType( "char", 8, llvm::dwarf::DW_ATE_unsigned );
	auto range = builder.getOrCreateArray( builder.getOrCreateSubrange( 0, bufferSize ) );
	auto type2 = builder.createArrayType( bufferSize, 8, type1, range );
	auto expr = builder.createGlobalVariableExpression( file, gv->getName(), gv->getName(), file, 0, type2, false );
	gv->addDebugInfo( expr );

	// Finalize the debug information, and configure module.
	builder.finalize();
	module.addModuleFlag( llvm::Module::Warning, "Debug Info Version", llvm::DEBUG_METADATA_VERSION );
}
