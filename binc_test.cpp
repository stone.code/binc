#include "vendor/catch2/catch.hpp"
#include <cstdio>
#include <string>

#include <llvm/ADT/Triple.h>
#include <llvm/IR/LLVMContext.h>
#include <llvm/IR/Module.h>
#include <llvm/Support/raw_ostream.h>

#include "binc.h"

#define QUOTEIMPL( str ) #str
#define QUOTE( str ) QUOTEIMPL( str )

using std::string;

static std::string readFile( char const* filename ) {
	auto file = std::unique_ptr<FILE, decltype( &fclose )>( fopen( filename, "rb" ), &fclose );
	REQUIRE( file.get() != nullptr );

	// Note:  The following does not handle text-mode files.  The translation
	// of newlines will lead to a mismatch in size of the file determined from
	// seeking, and then after reading.  This isn't an issue on linux, but on
	// Windows it causes this function to fail.
	auto rc = fseek( file.get(), 0, SEEK_END );
	REQUIRE( rc == 0 );
	auto dataSize = ftell( file.get() );
	rc = fseek( file.get(), 0, SEEK_SET );
	REQUIRE( rc == 0 );

	std::string buffer( dataSize, 0 );

	auto n = fread( const_cast<char*>( buffer.data() ), 1, dataSize, file.get() );
	REQUIRE( n == dataSize );
	return buffer;
}

static bool hasDebugInfo( llvm::GlobalVariable const& gv ) {
	llvm::SmallVector<llvm::DIGlobalVariableExpression*, 1> buffer;
	gv.getDebugInfo( buffer );
	return !buffer.empty();
}

TEST_CASE( "Build module", "[module][noIO]" ) {
	static struct {
		char const* filename;
		char const* varname;
		bool const nullTerminate;
		size_t const assetSize;
	} const cases[] = {
		{"./testdata/a.bin", "a", false, 0},
		{"./testdata/b.bin", "b", false, 10},
		{"./testdata/c.bin", "c", false, 21},
		{"./testdata/a.bin", "za", true, 0},
		{"./testdata/b.bin", "zb", true, 10},
		{nullptr, nullptr, false, 0}
	};

	for ( auto i = cases; i->filename; ++i ) {
		INFO( "Case " << ( i - cases ) );

		llvm::LLVMContext context;
		llvm::Module module( "binc", context );
		module.setSourceFileName( i->filename );
		auto const bufferSize = buildModule( module, i->varname, i->nullTerminate );
		CHECK( bufferSize );
		CHECK( bufferSize.get() == i->assetSize );

		REQUIRE( std::distance( module.global_begin(), module.global_end() ) == 1 );
		CHECK( module.global_begin()->getName() == i->varname );
		CHECK( module.global_begin()->isConstant() );
		REQUIRE( module.global_begin()->hasInitializer() );
		CHECK( module.global_begin()->getInitializer()->getType()->getArrayNumElements() ==
			   i->assetSize + ( i->nullTerminate ? 1 : 0 ) );

		CHECK( std::distance( module.alias_begin(), module.alias_end() ) == 1 );
		CHECK( module.alias_begin()->getName().startswith( i->varname ) );

		buildDebugInfo( module, bufferSize.get() );
		CHECK( hasDebugInfo( *module.global_begin() ) );
	}

	SECTION( "sad filename" ) {
		llvm::LLVMContext context;
		llvm::Module module( "binc", context );
		module.setSourceFileName( "dne.bin" );
		auto const bufferSize = buildModule( module, "dne", false );
		CHECK( !bufferSize );
		CHECK( bufferSize.getError() );

		CHECK( std::distance( module.global_begin(), module.global_end() ) == 0 );
	}
}

TEST_CASE( "Default arguments", "[]" ) {
	auto rc = system( QUOTE( CXX ) " -o ./testdata/stdout.o -g -c ./testdata/stdout.c" );
	REQUIRE( rc == 0 );

	SECTION( "round trip" ) {
		static struct {
			char const* filename;
			bool nullTerminate;
		} const cases[] = {
			{"./testdata/a.bin", false},
			{"./testdata/b.bin", false},
			{"./testdata/c.bin", false},
			{"./testdata/stdout.c", false},
			{"./testdata/a.bin", true},
			{"./testdata/b.bin", true},
			{nullptr}
		};

		for ( auto i = cases; i->filename; ++i ) {
			auto cmd = string( "./binc -o " ) + i->filename + ".o -c " + i->filename + " -n asset";
			auto rc = system( cmd.c_str() );
			CHECK( rc == 0 );

			cmd = string( QUOTE( CXX ) " -o ./testdata/stdout " ) + i->filename + ".o ./testdata/stdout.o";
			rc = system( cmd.c_str() );
			REQUIRE( rc == 0 );
			rc = system( "./testdata/stdout > tmp.bin" );
			REQUIRE( rc == 0 );

			auto want = readFile( i->filename );
			auto got = readFile( "tmp.bin" );
			CHECK( want == got );

			remove( "tmp.bin" );
			remove( "./testdata/stdout" );
			remove( ( string( i->filename ) + ".o" ).c_str() );
		}
	}

	SECTION( "version" ) {
		auto cmd = "./binc -version > tmp.bin";
		auto rc = system( cmd );
		CHECK( rc == 0 );

		auto got = readFile( "tmp.bin" );
		CHECK( got.compare( 0, 4, "binc" ) == 0 );

		remove( "tmp.bin" );
	}

	SECTION( "sad input filename" ) {
		auto cmd = "./binc -o tmp.o -c ./testdata/dne -n asset 2> tmp.bin";
		auto rc = system( cmd );
		CHECK( rc != 0 );

		auto got = readFile( "tmp.bin" );
		CHECK( got.compare( 0, 6, "error:" ) == 0 );

		remove( "tmp.bin" );
	}

	SECTION( "sad output filename" ) {
		auto cmd = "./binc -o . -c ./testdata/a.bin -n asset 2> tmp.bin";
		auto rc = system( cmd );
		CHECK( rc != 0 );

		auto got = readFile( "tmp.bin" );
		CHECK( got.compare( 0, 6, "error:" ) == 0 );

		remove( "tmp.bin" );
	}
}

TEST_CASE( "Build Object Files", "[]" ) {
		static struct {
			char const* cmd;
			char const* goldenfile;
		} const cases[] = {
			// Check that we can handle various triples
			{ "./binc -o tmp.o -c ./testdata/b.bin -filetype=ir -mtriple=x86_64-unknown-linux", "./testdata/b-x86_64-unknown-linux.il" },
			{ "./binc -o tmp.o -c ./testdata/b.bin -filetype=ir -mtriple=x86_64-pc-windows", "./testdata/b-x86_64-pc-windows.il" },
			// Check that we can add a null character
			{ "./binc -o tmp.o -c ./testdata/b.bin -filetype=ir -mtriple=x86_64-unknown-linux -z", "./testdata/bz-x86_64-unknown-linux.il" },
			// Check that we can output assembly
            #if LLVM_VERSION >= 1000
			{ "./binc -o tmp.o -c ./testdata/b.bin -filetype=asm -mtriple=x86_64-unknown-linux", "./testdata/b-x86_64-unknown-linux.llvm-10.s" },
            #else
			{ "./binc -o tmp.o -c ./testdata/b.bin -filetype=asm -mtriple=x86_64-unknown-linux", "./testdata/b-x86_64-unknown-linux.s" },
			#endif
            // Check that we can output with debug
			#if LLVM_VERSION >= 1100
			{ "./binc -o tmp.o -g -c ./testdata/b.bin -filetype=ir -mtriple=x86_64-unknown-linux", "./testdata/bg-x86_64-unknown-linux.llvm-11.ll" },
			#else
			{ "./binc -o tmp.o -g -c ./testdata/b.bin -filetype=ir -mtriple=x86_64-unknown-linux", "./testdata/bg-x86_64-unknown-linux.ll" },
			#endif
			{ nullptr, nullptr }
		};

		for ( auto i = cases; i->cmd; ++i ) {
			INFO( i->cmd );

			auto rc = system( i->cmd );
			CHECK( rc == 0 );

			auto want = readFile( i->goldenfile );
			auto got = readFile( "tmp.o" );
			CHECK( want == got );

			remove( "tmp.o" );
		}
}

TEST_CASE( "Output Filename", "[config][noIO]" ) {
	static struct {
		char const* inputFilename;
		CodeGenFileType cgft;
		char const* outputFilename;
	} const cases[] = {
		{"-", CGFT_AssemblyFile, "-"},
		{"-", CGFT_ObjectFile, "-"},
		{"-", CGFT_Null, "-"},
		{"-", CGFT_IR, "-"},
		{"./in.bin", CGFT_AssemblyFile, "./in.bin.s"},
		{"./in.bin", CGFT_ObjectFile, "./in.bin.o"},
		{"./in.bin", CGFT_Null, "./in.bin.null"},
		{"./in.bin", CGFT_IR, "./in.bin.ll"},
		{nullptr, CGFT_AssemblyFile, nullptr}
	};

	for ( auto i = cases; i->inputFilename; ++i ) {
		INFO( "Case " << ( i - cases ) );

		std::string out;
		getOutputFilename( out, i->inputFilename, i->cgft, "", llvm::Triple() );
		CHECK( i->outputFilename == out );
	}
}

TEST_CASE( "Variable Name", "[config][noIO]" ) {
	static struct {
		char const* inputName;
		char const* inputFilename;
		char const* variableName;
	} const cases[] = {
		{"var", "-", "var"},
		{"", "a.bin", "a_bin"},
		{nullptr, nullptr, nullptr}
	};

	for ( auto i = cases; i->inputName; ++i ) {
		INFO( "Case " << ( i - cases ) );

		std::string out( i->inputName );
		getVariableName( out, i->inputFilename );
		CHECK( i->variableName == out );
	}
}