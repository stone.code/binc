#include <assert.h>		// For assert
#include <stdio.h>		// For fputs and fputc
#include <stdlib.h>		// For EXIT_SUCCESS
#include <string.h>		// for strlen

extern char const myasset[];
extern char const myasset_end[];

int main( int argc, char const* argv[] ) {
	assert( myasset_end - myasset == strlen( myasset ) );

	fputs( myasset, stdout );
	if ( argc == 1 ) {
		fputc( '\n', stdout );
	}

	return EXIT_SUCCESS;
}
