	.text
	.file	"b.bin"
	.type	b_bin,@object
	.section	.rodata,"a",@progbits
	.globl	b_bin
b_bin:
	.ascii	"0123456789"
	.size	b_bin, 10

	.globl	b_bin_end
.set b_bin_end, b_bin+10
	.section	".note.GNU-stack","",@progbits
