#include <stdio.h>

extern char const asset[];
extern char const asset_end[];

int main( int argc, char const* argv[] ) {
	fwrite( asset, 1, asset_end - asset, stdout );
	return 0;
}
