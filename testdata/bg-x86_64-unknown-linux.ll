; ModuleID = 'binc'
source_filename = "./testdata/b.bin"
target triple = "x86_64-unknown-linux"

@b_bin = constant [10 x i8] c"0123456789", !dbg !0

@b_bin_end = alias i8, getelementptr inbounds ([10 x i8], [10 x i8]* @b_bin, i64 1, i64 0)

!llvm.dbg.cu = !{!7}
!llvm.module.flags = !{!10}

!0 = !DIGlobalVariableExpression(var: !1, expr: !DIExpression())
!1 = distinct !DIGlobalVariable(name: "b_bin", linkageName: "b_bin", scope: !2, file: !2, type: !3, isLocal: false, isDefinition: true)
!2 = !DIFile(filename: "./testdata/b.bin", directory: "")
!3 = !DICompositeType(tag: DW_TAG_array_type, baseType: !4, size: 10, align: 8, elements: !5)
!4 = !DIBasicType(name: "char", size: 8, encoding: DW_ATE_unsigned)
!5 = !{!6}
!6 = !DISubrange(count: 10)
!7 = distinct !DICompileUnit(language: DW_LANG_C, file: !2, producer: "binc", isOptimized: false, runtimeVersion: 0, emissionKind: FullDebug, enums: !8, globals: !9)
!8 = !{}
!9 = !{!0}
!10 = !{i32 2, !"Debug Info Version", i32 3}
