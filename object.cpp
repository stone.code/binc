#include <cstdio>
#include <string>

#include <llvm/CodeGen/MachineModuleInfo.h>
#include <llvm/IR/LegacyPassManager.h>
#include <llvm/IR/Module.h>
#include <llvm/Support/FileSystem.h>
#include <llvm/Support/ToolOutputFile.h>
#include <llvm/Target/TargetMachine.h>

#include "binc.h"

static bool requiresBinaryOutput( llvm::CodeGenFileType filetype ) {
	return filetype == llvm::CodeGenFileType::CGFT_ObjectFile || filetype == llvm::CodeGenFileType::CGFT_Null;
}

std::error_code writeOutputFile( llvm::Module& module, llvm::StringRef filename, llvm::CodeGenFileType fileType,
								 llvm::TargetMachine& targetMachine ) {
	// Open the output file
	std::error_code ec;
	llvm::sys::fs::OpenFlags openFlags =
		requiresBinaryOutput( fileType ) ? llvm::sys::fs::F_None : llvm::sys::fs::F_Text | llvm::sys::fs::F_None;
#if LLVM_VERSION >= 1000
	auto out = std::make_unique<llvm::ToolOutputFile>( filename, ec, openFlags );
#elif LLVM_VERSION >= 600
	auto out = llvm::make_unique<llvm::ToolOutputFile>( filename, ec, openFlags );
#else
	llvm::raw_fd_ostream out( filename, ec, openFlags );
#endif
	if ( ec ) {
		return ec;
	}

	// Create the machine
	auto& targetMachine2 = static_cast<llvm::LLVMTargetMachine&>( targetMachine );
#if LLVM_VERSION >= 1000
	auto machine = new llvm::MachineModuleInfoWrapperPass( &targetMachine2 );
#else
	auto machine = new llvm::MachineModuleInfo( &targetMachine2 );
#endif
	module.setDataLayout( targetMachine.createDataLayout() );

	// Run pases
	llvm::legacy::PassManager passManager;
#if LLVM_VERSION >= 700
	targetMachine.addPassesToEmitFile( passManager, out->os(), nullptr, fileType, false, machine );
#elif LLVM_VERSION >= 600
	targetMachine.addPassesToEmitFile( passManager, out->os(), fileType, false, machine );
#else
	targetMachine.addPassesToEmitFile( passManager, out, fileType, false, machine );
#endif
	passManager.run( module );
#if LLVM_VERSION >= 600
	out->keep();
#endif

	// Return, no error
	return {};
}
