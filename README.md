# Binary Asset Compiler
> Binary assets, compiled fast.

For many applications, it is worthwhile to embed binary assets into the final library or executable.  This can be a convenient method to package those assets for distribution.  The binary asset compiler converts any binary file directly into an object file, ready for linking.

With the exception of some non-portable approaches, binc is the best performing binary asset compiler tested.  It uses less memory, and compiles assets faster, than other approaches.  For details of the testing and analysis, please see [this post](http://stonecode.ca/posts/binc_benchmarks/).

## Getting started

To compile a binary asset into an object file, use the following command:

```shell
binc -c asset.bin -o asset.o -n varname
```

This will create an object file which contains a single instance, the embedded binary asset.  This embedded data can be accessed from, for example, a C program with the following declaration:

```C
extern char const varname[];
extern char const varname_end[];
```

There is an example program that simply prints the contents of the binary asset to standard output in the [example folder](https://gitlab.com/stone.code/binc/tree/master/example).

### Options

**-c=`filename`**  
Specify the input filename.  If -, use standard input (stdin).

**-code-model=`model`**  
Choose a code model.  One of:  small, kernel, medium, or large.

**-data-sections**  
Emit data into separate sections

**-filetype=`type`**  
Choose a file type for the output.  Not all types are supported by all targets.  One of: asm for an assembly ('.s') file, obj for a native object ('.o') file, null for nothing, or ir for an LLVM IR ('.ll') file.

**-march=`string`**  
Architecture to generate code for (see --version).

**-mattr=`a1,+a2,-a3,...`**  
Target specific attributes (-mattr=help for details).

**-mcpu=`cpu-name`**  
Target a specific cpu type (-mcpu=help for details).

**-mtriple=`string`**  
Override target triple for module.

**-n=`identifier`**  
Specify linkage name for binary asset.

**-o=`filename`**  
Specify the output filename.  If -, use standard output (stdout).

**-relocation-model=`model`**  
Choose relocation model.  One of:  static, pic, dynamic-no-pic, ropi, rwpi, or ropi-rwpi.

**-z**  
Add a null terminator to the binary asset.

### Cross-compiling

The binary asset compiler provides a complete set of command-line options for controlling cross compilation.  However, since there is no code generation, setting the target triple should be sufficient for most cases.  The following example builds object files for both Linux and WIN32.

```shell
binc -c asset.bin -o asset.o -n varname -mtriple x86_64-unknown-linux
binc -c asset.bin -o asset.obj -n varname -mtriple x86_64-pc-win32
```

## Installation

### From Source

To build the binary asset compiler, the [LLVM library](http://llvm.org/) must be available on the host system.  Any version of LLVM between 5 and 10 should be compatible.  Beware that the pre-built binaries available from the project provide the tools, but, depending on platform, not necessarily the headers and libraries required for development.

#### Debian

For debian-based systems, the following should build and install the compiler:

```shell
sudo apt-get install llvm-dev
cd [project-folder]
make
make install
```

There is little configuration for the build, as the compiler will match the configuration used to build LLVM.  By default, the makefile-based approach will use the program `llvm-config` to determine where LLVM is installed.  If you have installed other versions of LLVM, or would like to target a particular version, then you can override this parameter when calling make.

```shell
LLVM_CONFIG=llvm-config-8 make
```

The default location for installation is `/usr/local/bin`, but this can be changed by supplying a value for either PREFIX or BINDIR when calling `make install`.  Additionally, you will likely need superuser priviledges to complete the install.

Note that [CMake](https://cmake.org/) can also be used to build the project.

#### Windows

On a windows systems, you can install LLVM using the source code and [CMake](https://cmake.org/).  Please download the [source code](http://releases.llvm.org/).  Note that you will also need a C++ compiler (obviously) and CMake.

```shell
xz --decompress --stdout llvm-7.0.1.src.tar.xz | tar xf -
mkdir .\llvm-build
cd .\llvm-build
cmake -Thost=x64 -DLLVM_BUILD_TOOLS=OFF -DLLVM_INCLUDE_TESTS=OFF -DCMAKE_BUILD_TYPE=Release .\llvm-7.0.1.src
cmake --build . --config Release
cd ..
mkdir .\binc-build
cd .\binc-build
cmake [path to binc sources] -DLLVM_DIR=.\llvm-build\lib\cmake\llvm
cmake --build . --config Release
```

Note that the first line, which is used to decompress the archive with the source code for LLVM, assumes that the host machine has Cygwin installed, or some other package providing these utilities.  Another option is to use [7-Zip](https://www.7-zip.org/) from the command line.

#### MacOS

Since LLVM is available on MacOS, you should be able to build `binc` using either the makefile or with CMake.  Unfortunately, I don't have access to a MacOS system, so  building on MacOS is untested.  Pull requests with build instructions would be welcome.

## Contributing

Feature development of this project is complete.  It has not been released to version 1 because cross-compiling has not seen use in production, and the some of the command-line flags may be unnecessary, and so could be removed.

If you have find a bug or have any suggestions, please [open an issue](https://gitlab.com/stone.code/binc/issues).

If you'd like to contribute, please fork the repository and make changes.  Pull requests are welcome.

### Scope

The following are the priorities of the binary asset compiler project:

* Portable:  The tool will be usable on all major platforms, and be able to cross-compile assets to all major platforms.
* Fast:  The tool will compile assets quickly, limiting memory and IO overhead so that large assets can be compiled without undue delay.
* Focused:  The tool compiles binary assets into object files.  The generation of header files, compression, or managing multiple assets should be handled by the build system.

Please consider the project's scope when considering any feature requests, or when considering submitting a pull request.

## Related projects

- [xxd](http://www.wzw.tum.de/public-html/syring/win32/UnxUtilsDist.html):  Ports of common GNU utilities to native Win32.
- [file2c](https://bitbucket.org/rj/file2c/src):  Convert a binary file into a C source file.
- [bin2c](https://github.com/gwilymk/bin2c):  A very simple utility for converting a binary file to a C source file.
- [incbin](https://github.com/graphitemaster/incbin): Include binary files in your C/C++ applications using inline assembly.
- [honeycomb](https://gitlab.com/nishanthkarthik/honeycomb):  A tiny cross platform resource compiler.
- [objcopy](https://linux.die.net/man/1/objcopy): Copies the contents of one object file (or binary blob) to another object file.  Part of [binutils](https://www.gnu.org/software/binutils/).
- [llvm-objcopy](http://llvm.org/docs/CommandGuide/index.html):  Copies the contents of one object file (or binary blob) to another object file.  Part of [LLVM](https://www.llvm.org), and included since version 6, but not documented with the other commands.
- [std::embed](https://thephd.github.io/vendor/future_cxx/papers/d1040.html):  A proposal for C++ that adds a function that allows pulling resources at compile-time into a program.

A useful overview of the various approaches to compile binary assets can be found [here](https://www.devever.net/~hl/incbin).

## Licensing

This project is licensed under the [University of Illinois/NCSA Open Source License (NCSA)](https://opensource.org/licenses/NCSA).  See the LICENSE in the repository.
