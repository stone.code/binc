with import <nixpkgs> {};

stdenv.mkDerivation {
  name = "binc";
  src = ./.;

  buildInputs = [ llvm ];

  buildPhase = "make binc";

  installPhase = ''
    mkdir -p $out/bin
    cp binc $out/bin/
  '';
}