#ifndef BINC_BINC_H
#define BINC_BINC_H

#include <llvm/ADT/StringRef.h>
#include <llvm/Support/ErrorOr.h>

#if LLVM_VERSION >= 1000
#include <llvm/Support/CodeGen.h>
#else
#include <llvm/Target/TargetMachine.h>
namespace llvm {
typedef TargetMachine::CodeGenFileType CodeGenFileType;
}
#endif

namespace llvm {
// Forward declare
class Module;
class TargetMachine;
class Triple;
} // namespace llvm

enum CodeGenFileType {
	CGFT_AssemblyFile = llvm::CodeGenFileType::CGFT_AssemblyFile,
	CGFT_ObjectFile = llvm::CodeGenFileType::CGFT_ObjectFile,
	CGFT_Null = llvm::CodeGenFileType::CGFT_Null,
	CGFT_IR
};

extern llvm::StringRef getOutputFilename( std::string& outputFilename, llvm::StringRef inputFilename,
										  CodeGenFileType fileType, char const* targetName,
										  llvm::Triple const& triple );
extern llvm::StringRef getVariableName( std::string& variableName, llvm::StringRef inputFilename );

extern llvm::ErrorOr<size_t> buildModule( llvm::Module& module, llvm::StringRef variableName, bool nullTerminate );
extern void buildDebugInfo( llvm::Module& module, size_t bufferSize );
extern std::error_code writeOutputFile( llvm::Module& module, llvm::StringRef filename, llvm::CodeGenFileType fileType,
										llvm::TargetMachine& targetMachine );

#endif